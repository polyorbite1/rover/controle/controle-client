export interface Imu {
  x_inertia: number;
  y_inertia: number;
  z_inertia: number;
  velocity: number;
  acceleration: number;
}
