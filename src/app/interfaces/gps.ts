export interface Gps {
  longitude: string;
  latitude: string;
}
