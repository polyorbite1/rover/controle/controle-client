import { Imu } from './imu';
import { Lidar } from './lidar';
import { Gps } from './gps';

export interface Updates {
  battery: number;
  imu: Imu;
  lidar: Lidar;
  gps: Gps;
}
