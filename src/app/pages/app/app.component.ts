import { Component } from '@angular/core';
import { GamepadService } from 'src/app/services/gamepad/gamepad.service';
import { RosService } from 'src/app/services/ros/ros.service';
import { WebsocketService } from '../../services/websocket/websocket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'NGFlaskCRUD';

  constructor(
    // public wsService: WebsocketService,
    public gamepadService: GamepadService,
    public rosService: RosService
  ) {}
}
