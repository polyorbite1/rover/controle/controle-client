import { Injectable } from '@angular/core';
import * as ROSLIB from 'roslib';
import { Ros, Topic } from 'roslib';

@Injectable({
  providedIn: 'root',
})
export class RosService {
  ros = new Ros({ url: 'ws://localhost:9090' });
  // ros = new Ros({ url: 'ws://192.168.228.129:9090' });

  constructor() {
    this.connect();
    // this.sendSpeed();
  }

  connect(): void {
    this.handleConnectionToServer();
    this.handleError();
    this.handleClose();
    // const myTopic = this.getTopic('/cmd_vel', 'geometry_msgs/msg/Twist');
    // myTopic.subscribe((message) => {
    //   const myMessage = message as any;
    //   console.log(myMessage.data);
    // });
  }

  sendSpeed(): void {
    const speedTopic = this.getTopic('/cmd_vel', 'geometry_msgs/msg/Twist');
    const speed = new ROSLIB.Message({
      linear: {
        x: 500,
        y: 0,
        z: 0,
      },
      angular: {
        x: 0,
        y: 0,
        z: 0,
      },
    });
    setInterval(() => {
      speedTopic.publish(speed);
      // console.log('test');
    }, 10);
  }

  handleConnectionToServer(): void {
    this.ros.on('connection', () => {
      console.log('client connected to ros server');
    });
  }

  handleError(): void {
    this.ros.on('error', (error) => {
      console.log('ros server threw error', error);
    });
  }

  handleClose(): void {
    this.ros.on('close', () => {
      console.log('ros server shutdown');
    });
  }

  getTopic(name: string, messageType: string): Topic {
    return new Topic({ ros: this.ros, name, messageType });
  }
}
