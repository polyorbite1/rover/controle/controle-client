import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { GamepadHandler } from '../../classes/gamepad-handler/gamepad-handler';
import { RoverControlProxyService } from '../rover-control-proxy/rover-control-proxy.service';

declare global {
  interface WindowEventMap {
    gamepadconnected: GamepadEvent;
  }
}

@Injectable({
  providedIn: 'root',
})
export class GamepadService {
  private handlers: Map<string, GamepadHandler>;

  constructor(controlProxy: RoverControlProxyService) {
    this.handlers = new Map<string, GamepadHandler>();

    fromEvent(window, 'gamepadconnected').subscribe((event: Event) => {
      const gamepadEvent = event as any as GamepadEvent;
      const id = gamepadEvent.gamepad.id;
      const index = gamepadEvent.gamepad.index;
      this.handlers.set(id, new GamepadHandler(index, controlProxy));
    });

    fromEvent(window, 'gamepaddisconnected').subscribe((event: Event) => {
      const gamepadEvent = event as any as GamepadEvent;
      this.handlers.delete(gamepadEvent.gamepad.id);
    });
  }

  get gamepad(): Gamepad {
    return navigator.getGamepads() as unknown as Gamepad;
  }
}
