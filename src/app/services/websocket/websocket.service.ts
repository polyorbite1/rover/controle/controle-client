import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { io } from 'socket.io-client';
import { Updates } from 'src/app/interfaces/updates';

const UPDATES_INTERVAL_MS = 10;

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  socket = io('http://127.0.0.1:5000');
  updates$: Subject<Updates>;

  constructor() {
    console.log('websocket service constructed');
    this.updates$ = new Subject<Updates>();
    this.connect();
  }

  connect(): void {
    this.handleAfterServerConnect();
    this.askUpdates();
    this.handleUpdates();
  }

  handleAfterServerConnect(): void {
    this.socket.on('afterServerConnect', () =>
      console.log('client connected to server')
    );
  }

  askUpdates(): void {
    setInterval(() => this.socket.emit('getUpdates'), UPDATES_INTERVAL_MS);
  }

  handleUpdates(): void {
    this.socket.on('newUpdates', (updates) => {
      this.updates$.next(updates);
    });
  }
}
