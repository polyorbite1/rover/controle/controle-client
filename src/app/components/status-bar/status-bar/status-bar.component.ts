import { Component, OnInit } from '@angular/core';
import { WebsocketService } from 'src/app/services/websocket/websocket.service';

const BATTERY_100_PERCENT_ICON =
  'https://static.thenounproject.com/png/447482-200.png';
const BATTERY_80_PERCENT_ICON =
  'https://static.thenounproject.com/png/447480-200.png';
const BATTERY_60_PERCENT_ICON =
  'https://static.thenounproject.com/png/447484-200.png';
const BATTERY_40_PERCENT_ICON =
  'https://static.thenounproject.com/png/447483-200.png';
const BATTERY_20_PERCENT_ICON =
  'https://static.thenounproject.com/png/447479-200.png';
const BATTERY_0_PERCENT_ICON =
  'https://static.thenounproject.com/png/447485-200.png';
const BATTERY_CHARGING_ICON =
  'https://static.thenounproject.com/png/447481-200.png';
const BATTERY_UNKNOWN_ICON =
  'https://static.thenounproject.com/png/447486-200.png';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.css'],
})
export class StatusBarComponent implements OnInit {
  batteryPercentage: number = -1;
  batteryIconSrc: string = BATTERY_UNKNOWN_ICON;

  constructor(public wsService: WebsocketService) {
    this.wsService.updates$.subscribe((updates) => {
      this.batteryPercentage = updates.battery;
      this.updateBatteryIcon();
    });
  }

  updateBatteryIcon(): void {
    if (isNaN(this.batteryPercentage as number))
      this.batteryIconSrc = BATTERY_UNKNOWN_ICON;
    else if (this.batteryPercentage === 100)
      this.batteryIconSrc = BATTERY_100_PERCENT_ICON;
    else if (this.batteryPercentage === 0)
      this.batteryIconSrc = BATTERY_0_PERCENT_ICON;
    else if (90 <= this.batteryPercentage && this.batteryPercentage < 100)
      this.batteryIconSrc = BATTERY_100_PERCENT_ICON;
    else if (70 <= this.batteryPercentage && this.batteryPercentage < 90)
      this.batteryIconSrc = BATTERY_80_PERCENT_ICON;
    else if (50 <= this.batteryPercentage && this.batteryPercentage < 70)
      this.batteryIconSrc = BATTERY_60_PERCENT_ICON;
    else if (30 <= this.batteryPercentage && this.batteryPercentage < 50)
      this.batteryIconSrc = BATTERY_40_PERCENT_ICON;
    else this.batteryIconSrc = BATTERY_20_PERCENT_ICON;
  }

  isValidPercentage(value: any): boolean {
    return 0 <= value && value <= 100;
  }

  ngOnInit(): void {}
}
