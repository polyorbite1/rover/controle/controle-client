import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraMinimapComponent } from './camera-minimap.component';

describe('CameraMinimapComponent', () => {
  let component: CameraMinimapComponent;
  let fixture: ComponentFixture<CameraMinimapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CameraMinimapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraMinimapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
