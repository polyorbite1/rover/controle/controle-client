import { Component, OnInit } from '@angular/core';
import { Gps } from 'src/app/interfaces/gps';
import { WebsocketService } from 'src/app/services/websocket/websocket.service';

declare const L: any;
const DEFAULT_COORDINATES: Gps = { latitude: '0', longitude: '0' };

@Component({
  selector: 'app-camera-minimap',
  templateUrl: './camera-minimap.component.html',
  styleUrls: ['./camera-minimap.component.css'],
})
export class CameraMinimapComponent implements OnInit {
  currentTopic: string = 'assets/images/example_camera.jpg';
  mapState: boolean = true;
  gpsCoordinates: Gps = {
    latitude: DEFAULT_COORDINATES.latitude,
    longitude: DEFAULT_COORDINATES.longitude,
  };
  map: any;

  constructor(public wsService: WebsocketService) {}

  ngOnInit(): void {
    this.updateMap();
    this.wsService.updates$.subscribe((updates) => {
      if (this.isNewGpsCoordinates(updates.gps)) {
        this.updateGpsCoordinates(updates.gps);
        this.map = this.map.remove();
        this.updateMap();
      }
    });
  }

  updateMap(): void {
    this.map = L.map('map').setView(
      [this.gpsCoordinates.latitude, this.gpsCoordinates.longitude],
      13
    );
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution:
        '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }).addTo(this.map);
    let roverMarker = L.marker([
      this.gpsCoordinates.latitude,
      this.gpsCoordinates.longitude,
    ]).addTo(this.map);
    roverMarker.bindPopup('<b>Rover</b>').openPopup();
  }

  updateGpsCoordinates(gpsCoordinates: Gps): void {
    this.gpsCoordinates.latitude = gpsCoordinates.latitude;
    this.gpsCoordinates.longitude = gpsCoordinates.longitude;
  }

  isNewGpsCoordinates(gpsCoordinates: Gps): boolean {
    return (
      gpsCoordinates.latitude !== this.gpsCoordinates.latitude ||
      gpsCoordinates.longitude !== this.gpsCoordinates.longitude
    );
  }

  toggleMap() {
    this.mapState = !this.mapState;
  }
}
