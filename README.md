# controle client

## Development Setup

### Prerequisites

- Install [Node.js] which includes [Node Package Manager][npm]

Install the Angular CLI globally:

```
npm install -g @angular/cli
```

### Install the project dependencies

```
npm install
```

### Run the project on local host port 4200

```
npm start
```
